PERSONAL VIMRC
==============

# Installation

Start with no .vim directory or .vimrc file in your home directory.  First, run
vim-plug installation step:

```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

Next, clone this repo in your home directory and create symlinks to the files in this repo.

```
git clone https://gitlab.com/aaronm04/vimrc vimrc-repo
ln -s vimrc-repo/dot_vimrc ~/.vimrc
```

# Setup

```
:PlugInstall
```

# Dependencies

* Vim 8+ compiled with Ruby (and probably other requirements like Perl/Python)

* [universal-ctags](https://ctags.io). You will need to update `~/.vimrc` with the path to universal-ctags. Search for `ctagsbin`.

  **macOS:** `brew install --HEAD universal-ctags/universal-ctags/universal-ctags` to get it at `/usr/local/bin/ctags`.

  **Fedora:** [Install via Snap](https://snapcraft.io/install/universal-ctags/fedora) with `sudo snap install universal-ctags` to get it at `/snap/universal-ctags/current/bin/ctags`.

  **OpenBSD:** `doas pkg_add universal-ctags`.

* [rust](https://rustup.rs/)

* [rust-analyzer](https://github.com/rust-analyzer/rust-analyzer)

* [fzf](https://github.com/junegunn/fzf)

* [ripgrep](https://github.com/BurntSushi/ripgrep)

* [Go](https://golang.org) with gopls and golangci-lint.

* [msgpack for Python](https://pypi.org/project/msgpack/), required for Deoplete code completion. Try: `pip3 install msgpack` or installing the `py3-msgpack` package (on OpenBSD at least).

* [pynvim for Python](https://pypi.org/project/pynvim/), required for Deoplete code completion. Try `pip3 install pynvim`.

* [solargraph](https://solargraph.org/guides/getting-started), required for Ruby language support. Try `gem install solargraph`. If necessary, also change `g:ale_ruby_solargraph_executable` to the rbenv path. It may also be useful to check out the instructions for the [`solargraph-rails` repository](https://github.com/iftheshoefritz/solargraph-rails).

* [standardrb](https://github.com/testdouble/standard), required for Ruby linting. Try `gem install standard`. See also `g:ale_ruby_standardrb_executable`.

* [rubocop](https://rubocop.org/), required for Ruby linting, although I am starting to dislike it... Try `gem install rubocop`. See also `g:ale_ruby_rubocop_executable`.

# Go Tools Installation

Install `gopls`:

```
go install golang.org/x/tools/gopls@latest
```

Install `golangci-lint` using either the following command or the [official instructions](https://golangci-lint.run/usage/install/#local-installation):

```
go install github.com/golangci/golangci-lint/cmd/golangci-lint@v1.47.1
```
